DROP TABLE unidades_saude IF EXISTS;
DROP TABLE funcionarios IF EXISTS;
DROP TABLE pacientes IF EXISTS;
DROP TABLE prontuarios IF EXISTS;
DROP TABLE tipo_teste IF EXISTS;
DROP TABLE testes IF EXISTS;
DROP TABLE roles IF EXISTS;
DROP TABLE users IF EXISTS;

CREATE TABLE unidades_saude (
  id INTEGER IDENTITY PRIMARY KEY,
  nome VARCHAR(100),
  endereco  VARCHAR(100),
  cep VARCHAR(8),
  telefone VARCHAR(10)
);


CREATE TABLE funcionarios (
  id INTEGER IDENTITY PRIMARY KEY,
  nome_completo VARCHAR(100),
  rg VARCHAR(10),
  cpf VARCHAR(11),
  endereco VARCHAR(100),
  cep VARCHAR(8),
  sexo VARCHAR(20),
  telefone VARCHAR(10),
  data_nascimento DATE,
  setor VARCHAR(30),
  funcao VARCHAR(45), 
  unidade_saude_id INTEGER NOT NULL
);
ALTER TABLE funcionarios ADD CONSTRAINT fk_funcionarios_us FOREIGN KEY (unidade_saude_id) REFERENCES unidades_saude (id);
CREATE INDEX funcionarios_cpf ON funcionarios (cpf);


CREATE TABLE pacientes (
  id INTEGER IDENTITY PRIMARY KEY,
  nome_completo VARCHAR(100),
  rg VARCHAR(10),
  cpf VARCHAR(11),
  endereco VARCHAR(100),
  cep VARCHAR(8),
  sexo VARCHAR(20),
  telefone VARCHAR(10),
  data_nascimento DATE,
  cns VARCHAR(15),
  nome_mae VARCHAR(50),
  obs VARCHAR(100),
  nome_responsavel VARCHAR(50),
  cpf_responsavel VARCHAR(11),
  telefone_responsavel VARCHAR(10)
);
CREATE INDEX pacientes_cpf ON pacientes (cpf);

CREATE TABLE prontuarios (
  id INTEGER IDENTITY PRIMARY KEY,
  prontuarios_data DATE,
  funcionario_id INTEGER NOT NULL,
  paciente_id INTEGER NOT NULL
);
ALTER TABLE prontuarios ADD CONSTRAINT fk_prontuarios_funcionario FOREIGN KEY (funcionario_id) REFERENCES funcionarios (id);
ALTER TABLE prontuarios ADD CONSTRAINT fk_prontuarios_paciente FOREIGN KEY (paciente_id) REFERENCES pacientes (id);

CREATE TABLE tipo_teste (
  id   INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(80)
);
CREATE INDEX tipos_name ON tipo_teste (name);


CREATE TABLE testes (
  id         INTEGER IDENTITY PRIMARY KEY,
  resultado VARCHAR(30),
  tipo_id  INTEGER NOT NULL,
  prontuario_id INTEGER NOT NULL
);
ALTER TABLE testes ADD CONSTRAINT fk_testes_tipo FOREIGN KEY (tipo_id) REFERENCES tipo_teste (id);
ALTER TABLE testes ADD CONSTRAINT fk_testes_prontuario FOREIGN KEY (prontuario_id) REFERENCES prontuarios (id);

CREATE  TABLE users (
  username    VARCHAR(20) NOT NULL ,
  password    VARCHAR(20) NOT NULL ,
  enabled     BOOLEAN DEFAULT TRUE NOT NULL ,
  PRIMARY KEY (username)
);

CREATE TABLE roles (
  id              INTEGER IDENTITY PRIMARY KEY,
  username        VARCHAR(20) NOT NULL,
  role            VARCHAR(20) NOT NULL
);
ALTER TABLE roles ADD CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES users (username);
CREATE INDEX fk_username_idx ON roles (username);

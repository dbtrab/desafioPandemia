INSERT INTO unidades_saude VALUES (1, 'São Luís', 'Rua Ângelo Romano, 186 - Presidente Dutra', '14060680', '5136224437');
INSERT INTO unidades_saude VALUES (2, 'São Francisco', 'Rua Itu, 1.120 - Vila Carvalho', '14075090', '5136267423');
INSERT INTO unidades_saude VALUES (3, 'SOS Saúde', 'Rua Pedro Colino, no 271 - Res. Léo Gomes de Moraes', '14079044', '5136280030');
INSERT INTO unidades_saude VALUES (4, 'São Gabriel', 'Rua Cruz e Souza, 3.100 - Parque Ribeirão Preto', '14031560', '5136376224');
INSERT INTO unidades_saude VALUES (5, 'Núcleo Esperança', 'Rua Roberto Michellin, 95 - Antônio Marincek', '14061190', '5136226828');

INSERT INTO funcionarios VALUES (1, 'Maria Aparecida de Souza','225711485', '84858059090' , 'Rua Genoveva Onofre Barban, 851', '14061190','FEMININO' , '5136397490', '2020-07-05', 'Triagem', 'Enfermeiro', 3);
INSERT INTO funcionarios VALUES (2, 'Marisa Marchetti','222311425','84838053090', 'Av. Cavalheiro Paschoal Innecchi, 500, Jardim Independência', '14061190','FEMININO' , '515551523','2020-07-05', 'Triagem', 'Enfermeiro', 1);
INSERT INTO funcionarios VALUES (3, 'Ana Carolina Rodrigues','227811455','84876553090', 'Rua Genoveva Onofre Barban, 851', '14061100','FEMININO' , '515551165','2020-07-05', 'Consultas', 'Medico', 2);
INSERT INTO funcionarios VALUES (4, 'Ronaldo Souza Cavalcante','327834355','43876553430', 'Rua da Praia, 124 - Cidade Alta', '14061180','MASCULINO' , '515551999','2020-07-05', 'Triagem', 'Enfermeiro', 4);
INSERT INTO funcionarios VALUES (5, 'Francisco Silva','326534355','43876873130', 'Rua dos Navegantes, 81 - Vila Nova', '14081390','FEMININO', '515556524','2020-07-05', 'Consultas', 'Medico', 3);
INSERT INTO funcionarios VALUES (6, 'Tatiana Pereira da Silva','326534355','43876873130', 'Travessa Antonio de Souza, 1002, Medianeira', '14061190','FEMININO', '515551524','2020-07-05', 'Triagem', 'Enfermeiro', 2);

INSERT INTO pacientes VALUES (1, 'George','222311425','84838034090', '110 W. Liberty St.', '14061190','FEMININO','515551375','2020-07-05', '182345678977656', 'Rosana','Convoluções', 'Giovana', '98787656789', '515558145');
INSERT INTO pacientes VALUES (2, 'Betty','222311425','8548034090', '638 Cardinal Ave.', '14061190','FEMININO', '515554479','2020-07-05',     '188467388093777', 'Joana', 'Diabetes', 'Maria', '09897876567', '515556667');
INSERT INTO pacientes VALUES (3, 'Harold','222311425','8423053450', '563 Friendly St.', '14060680','FEMININO', '515553284','2020-07-05',     '160098004788367', 'Dalila', 'Hipertensão, Diabetes', 'Pedro', '12345654532', '515558576');
INSERT INTO pacientes VALUES (4, 'Peter','222311425','84838054590', '2387 S. Fair Way', '14075090','FEMININO', '515556524','2020-07-05',   '164453009807785', 'Maria', 'Alergia a amoxicilina', 'Claudia', '67856478987', '515551999');
INSERT INTO pacientes VALUES (5, 'Jean','222311425','84833253054', '105 N. Lake St.', '14061100','FEMININO', '515550296','2020-07-05',      '182344323454349', 'Darlene', 'Asma', 'Roberto', '11235678556', '515552325');
INSERT INTO pacientes VALUES (6, 'Maria','222311425','84838043590','345 Maple St.', '14081390','FEMININO', '515557282', '2020-07-05',     '183264738546474', 'Rosana',  'Bronquite', 'Roberto', '87886556756', '515558131');

INSERT INTO prontuarios VALUES (1,'2020-07-05', 1,1);
INSERT INTO prontuarios VALUES (2,'2020-07-05', 2,2);
INSERT INTO prontuarios VALUES (3,'2020-07-05', 3,3);
INSERT INTO prontuarios VALUES (4,'2020-07-04', 3,4);
INSERT INTO prontuarios VALUES (5,'2020-07-05', 2,5);
INSERT INTO prontuarios VALUES (6,'2020-07-04', 3,6);

INSERT INTO tipo_teste VALUES (1, 'Rapido');
INSERT INTO tipo_teste VALUES (2, 'RT-PCR');
INSERT INTO tipo_teste VALUES (3, 'Sorologia');

INSERT INTO testes VALUES (1, 'Negativo', 1, 1);
INSERT INTO testes VALUES (2, 'Negativo', 2, 2);
INSERT INTO testes VALUES (3, 'Positivo', 3, 3);
INSERT INTO testes VALUES (4, 'Postivo', 1, 4);
INSERT INTO testes VALUES (5, 'Positivo', 1, 5);
INSERT INTO testes VALUES (6, 'Negativo', 2, 3);
INSERT INTO testes VALUES (7, 'Inconclusivo', 1, 4);
INSERT INTO testes VALUES (8, 'Positivo', 2, 3);

INSERT INTO users(username,password,enabled) VALUES ('admin','{noop}admin', true);

INSERT INTO roles (username, role) VALUES ('admin', 'ROLE_UNIDADESAUDE');
INSERT INTO roles (username, role) VALUES ('admin', 'ROLE_FUNCIONARIO');
INSERT INTO roles (username, role) VALUES ('admin', 'ADMIN');

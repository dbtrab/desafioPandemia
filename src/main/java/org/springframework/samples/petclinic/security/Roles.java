package org.springframework.samples.petclinic.security;

import org.springframework.stereotype.Component;

@Component
public class Roles {

    public final String UNIDADESAUDE = "ROLE_UNIDADESAUDE";
    public final String FUNCIONARIO= "ROLE_FUNCIONARIO";
    public final String ADMIN = "ADMIN";
}

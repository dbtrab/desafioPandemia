/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.service;

import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Funcionario;
import org.springframework.samples.petclinic.model.UnidadeSaude;
import org.springframework.samples.petclinic.model.Paciente;
import org.springframework.samples.petclinic.model.Prontuario;
import org.springframework.samples.petclinic.model.TipoTeste;
import org.springframework.samples.petclinic.model.Teste;

/**
 * Mostly used as a facade so all controllers have a single point of entry
 *
 * @author Michael Isvy
 * @author Vitaliy Fedoriv
 */
public interface ClinicService {

	Funcionario findFuncionarioById(int id) throws DataAccessException;
	Collection<Funcionario> findAllFuncionarios() throws DataAccessException;
	void saveFuncionario(Funcionario funcionario) throws DataAccessException;
	void deleteFuncionario(Funcionario funcionario) throws DataAccessException;

    UnidadeSaude findUnidadeSaudeById(int unidadeSaudeId);
    Collection<UnidadeSaude> findAllUnidadesSaude() throws DataAccessException;
    void saveUnidadeSaude(UnidadeSaude unidadeSaude) throws DataAccessException;
	void deleteUnidadeSaude(UnidadeSaude unidadeSaude) throws DataAccessException;
    Collection<UnidadeSaude> findUnidadeSaudeByName(String nome) throws DataAccessException;

    Paciente findPacienteById(int pacienteId);
	Collection<Paciente> findAllPacientes() throws DataAccessException;
	void savePaciente(Paciente paciente) throws DataAccessException;
    void deletePaciente(Paciente paciente) throws DataAccessException;
    Collection<Paciente> findPacienteByName(String nome) throws DataAccessException;

    Prontuario findProntuarioById(int prontuarioId);
    Collection<Prontuario> findAllProntuarios() throws DataAccessException;
    void saveProntuario(Prontuario prontuario) throws DataAccessException;
	void deleteProntuario(Prontuario prontuario) throws DataAccessException;

    TipoTeste findTipoTesteById(int tipoTesteId);
	Collection<TipoTeste> findAllTipoTestes() throws DataAccessException;
	Collection<TipoTeste> findTipoTestes() throws DataAccessException;
	void saveTipoTeste(TipoTeste tipoTeste) throws DataAccessException;
    void deleteTipoTeste(TipoTeste tipoTeste) throws DataAccessException;

    Teste findTesteById(int id) throws DataAccessException;
	Collection<Teste> findAllTestes() throws DataAccessException;
	void saveTeste(Teste teste) throws DataAccessException;
	void deleteTeste(Teste teste) throws DataAccessException;


}

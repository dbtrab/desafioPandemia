/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.samples.petclinic.model.UnidadeSaude;
import org.springframework.samples.petclinic.model.Paciente;
import org.springframework.samples.petclinic.model.Prontuario;
import org.springframework.samples.petclinic.model.Teste;
import org.springframework.samples.petclinic.model.TipoTeste;
import org.springframework.samples.petclinic.model.Funcionario;
import org.springframework.samples.petclinic.repository.TesteRepository;
import org.springframework.samples.petclinic.repository.TipoTesteRepository;
import org.springframework.samples.petclinic.repository.UnidadeSaudeRepository;
import org.springframework.samples.petclinic.repository.PacienteRepository;
import org.springframework.samples.petclinic.repository.ProntuarioRepository;
import org.springframework.samples.petclinic.repository.FuncionarioRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Mostly used as a facade for all Petclinic controllers
 * Also a placeholder for @Transactional and @Cacheable annotations
 *
 * @author Michael Isvy
 * @author Vitaliy Fedoriv
 */
@Service

public class ClinicServiceImpl implements ClinicService {


    private UnidadeSaudeRepository unidadeSaudeRepository;
    private TesteRepository testeRepository;
    private TipoTesteRepository tipoTesteRepository;
    private PacienteRepository pacienteRepository;
    private ProntuarioRepository prontuarioRepository;
    private FuncionarioRepository funcionarioRepository;


    @Autowired
     public ClinicServiceImpl(
             UnidadeSaudeRepository unidadeSaudeRepository,
             TesteRepository testeRepository,
             TipoTesteRepository tipoTesteRepository,
             PacienteRepository pacienteRepository,
             ProntuarioRepository prontuarioRepository,
             FuncionarioRepository funcionarioRepository) {
        this.testeRepository = testeRepository;
        this.tipoTesteRepository = tipoTesteRepository;
        this.unidadeSaudeRepository = unidadeSaudeRepository;
        this.pacienteRepository = pacienteRepository;
        this.prontuarioRepository = prontuarioRepository;
        this.funcionarioRepository = funcionarioRepository;
    }

    @Override
	@Transactional(readOnly = true)
	public UnidadeSaude findUnidadeSaudeById(int unidadeSaudeId) {
		return unidadeSaudeRepository.findById(unidadeSaudeId);
    }

    @Override
	@Transactional(readOnly = true)
	public Collection<UnidadeSaude> findAllUnidadesSaude() throws DataAccessException {
		return unidadeSaudeRepository.findAll();
    }

    @Override
	@Transactional
	public void saveUnidadeSaude(UnidadeSaude unidadeSaude) throws DataAccessException {
		unidadeSaudeRepository.save(unidadeSaude);

    }

    @Override
	@Transactional
	public void deleteUnidadeSaude(UnidadeSaude unidadeSaude) throws DataAccessException {
		unidadeSaudeRepository.delete(unidadeSaude);

    }


    @Override
	@Transactional(readOnly = true)
	public Collection<UnidadeSaude> findUnidadeSaudeByName(String nome) throws DataAccessException {
		return unidadeSaudeRepository.findByName(nome);
	}


    @Override
	@Transactional(readOnly = true)
	public Paciente findPacienteById(int pacienteId) {
		return pacienteRepository.findById(pacienteId);
    }

    @Override
	@Transactional(readOnly = true)
	public Collection<Paciente> findAllPacientes() throws DataAccessException {
		return pacienteRepository.findAll();
    }

    @Override
	@Transactional
	public void savePaciente(Paciente paciente) throws DataAccessException {
		pacienteRepository.save(paciente);

    }

    @Override
	@Transactional
	public void deletePaciente(Paciente paciente) throws DataAccessException {
		pacienteRepository.delete(paciente);

    }

    @Override
	@Transactional(readOnly = true)
	public Collection<Paciente> findPacienteByName(String nome) throws DataAccessException {
		return pacienteRepository.findByName(nome);
	}

    @Override
	@Transactional(readOnly = true)
	public Prontuario findProntuarioById(int prontuarioId) {
		return prontuarioRepository.findById(prontuarioId);
    }

    @Override
	@Transactional(readOnly = true)
	public Collection<Prontuario> findAllProntuarios() throws DataAccessException {
		return prontuarioRepository.findAll();
    }

    @Override
	@Transactional
	public void saveProntuario(Prontuario prontuario) throws DataAccessException {
		prontuarioRepository.save(prontuario);

    }

    @Override
	@Transactional
	public void deleteProntuario(Prontuario prontuario) throws DataAccessException {
		prontuarioRepository.delete(prontuario);

    }

    @Override
	@Transactional(readOnly = true)
	public TipoTeste findTipoTesteById(int tipoTesteId) {
		return tipoTesteRepository.findById(tipoTesteId);
    }


    @Override
	@Transactional(readOnly = true)
	public Collection<TipoTeste> findTipoTestes() throws DataAccessException {
		return testeRepository.findTipoTestes();
	}

    @Override
	@Transactional(readOnly = true)
	public Collection<TipoTeste> findAllTipoTestes() throws DataAccessException {
		return tipoTesteRepository.findAll();
    }

    @Override
	@Transactional
	public void saveTipoTeste(TipoTeste tipoTeste) throws DataAccessException {
		tipoTesteRepository.save(tipoTeste);

    }

    @Override
	@Transactional
	public void deleteTipoTeste(TipoTeste tipoTeste) throws DataAccessException {
		tipoTesteRepository.delete(tipoTeste);

    }

    @Override
	@Transactional(readOnly = true)
	public Teste findTesteById(int testeId) {
		return testeRepository.findById(testeId);
    }
    @Override
	@Transactional(readOnly = true)
	public Collection<Teste> findAllTestes() throws DataAccessException {
		return testeRepository.findAll();
    }

    @Override
	@Transactional
	public void saveTeste(Teste teste) throws DataAccessException {
		testeRepository.save(teste);

    }

    @Override
	@Transactional
	public void deleteTeste(Teste teste) throws DataAccessException {
		testeRepository.delete(teste);

    }

    @Override
	@Transactional(readOnly = true)
	public Funcionario findFuncionarioById(int funcionarioId) {
		return funcionarioRepository.findById(funcionarioId);
    }

    @Override
	@Transactional(readOnly = true)
	public Collection<Funcionario> findAllFuncionarios() throws DataAccessException {
		return funcionarioRepository.findAll();
    }

    @Override
	@Transactional
	public void saveFuncionario(Funcionario funcionario) throws DataAccessException {
		funcionarioRepository.save(funcionario);

    }

    @Override
	@Transactional
	public void deleteFuncionario(Funcionario funcionario) throws DataAccessException {
		funcionarioRepository.delete(funcionario);

    }


}

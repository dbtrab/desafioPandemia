/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.rest;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.TipoTeste;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/tipoTestes")
public class TipoTesteRestController {

	@Autowired
	private ClinicService clinicService;

    @PreAuthorize( "hasAnyRole(@roles.ADMIN, @roles.UNIDADESAUDE, @roles.FUNCIONARIO)" )
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<TipoTeste>> getAllTipoTestes(){
		Collection<TipoTeste> tipoTestes = new ArrayList<TipoTeste>();
		tipoTestes.addAll(this.clinicService.findAllTipoTestes());
		if (tipoTestes.isEmpty()){
			return new ResponseEntity<Collection<TipoTeste>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<TipoTeste>>(tipoTestes, HttpStatus.OK);
	}

    @PreAuthorize( "hasAnyRole(@roles.ADMIN, @roles.UNIDADESAUDE, @roles.FUNCIONARIO)" )
	@RequestMapping(value = "/{tipoTesteId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<TipoTeste> getTipoTeste(@PathVariable("tipoTesteId") int tipoTesteId){
		TipoTeste tipoTeste = this.clinicService.findTipoTesteById(tipoTesteId);
		if(tipoTeste == null){
			return new ResponseEntity<TipoTeste>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<TipoTeste>(tipoTeste, HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<TipoTeste> addTipoTeste(@RequestBody @Valid TipoTeste tipoTeste, BindingResult bindingResult, UriComponentsBuilder ucBuilder){
		BindingErrorsResponse errors = new BindingErrorsResponse();
		HttpHeaders headers = new HttpHeaders();
		if(bindingResult.hasErrors() || (tipoTeste == null)){
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<TipoTeste>(headers, HttpStatus.BAD_REQUEST);
		}
		this.clinicService.saveTipoTeste(tipoTeste);
		headers.setLocation(ucBuilder.path("/api/tipoTestes/{id}").buildAndExpand(tipoTeste.getId()).toUri());
		return new ResponseEntity<TipoTeste>(tipoTeste, headers, HttpStatus.CREATED);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/{tipoTesteId}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<TipoTeste> updateTipoTeste(@PathVariable("tipoTesteId") int tipoTesteId, @RequestBody @Valid TipoTeste tipoTeste, BindingResult bindingResult){
		BindingErrorsResponse errors = new BindingErrorsResponse();
		HttpHeaders headers = new HttpHeaders();
		if(bindingResult.hasErrors() || (tipoTeste == null)){
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<TipoTeste>(headers, HttpStatus.BAD_REQUEST);
		}
		TipoTeste currentTipoTeste = this.clinicService.findTipoTesteById(tipoTesteId);
		if(currentTipoTeste == null){
			return new ResponseEntity<TipoTeste>(HttpStatus.NOT_FOUND);
		}
		currentTipoTeste.setName(tipoTeste.getName());
		this.clinicService.saveTipoTeste(currentTipoTeste);
		return new ResponseEntity<TipoTeste>(currentTipoTeste, HttpStatus.NO_CONTENT);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/{tipoTesteId}", method = RequestMethod.DELETE, produces = "application/json")
	@Transactional
	public ResponseEntity<Void> deleteTipoTeste(@PathVariable("tipoTesteId") int tipoTesteId){
		TipoTeste tipoTeste = this.clinicService.findTipoTesteById(tipoTesteId);
		if(tipoTeste == null){
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		this.clinicService.deleteTipoTeste(tipoTeste);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}

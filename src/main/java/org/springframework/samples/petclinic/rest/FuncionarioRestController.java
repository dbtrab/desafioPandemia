package org.springframework.samples.petclinic.rest;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.Funcionario;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
/**
 * @author Eduarda Keller
 *
 */

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/funcionarios")

public class FuncionarioRestController {

	@Autowired
	private ClinicService clinicService;

    @PreAuthorize( "hasRole(@roles.US_ADMIN)" )
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<Funcionario>> getAllFuncionario(){
		Collection<Funcionario> funcionario = new ArrayList<Funcionario>();
		funcionario.addAll(this.clinicService.findAllFuncionarios());
		if (funcionario.isEmpty()){
			return new ResponseEntity<Collection<Funcionario>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<Funcionario>>(funcionario, HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.US_ADMIN)" )
	@RequestMapping(value = "/{funcionarioId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Funcionario> getFuncionario(@PathVariable("funcionarioId") int funcionarioId){
    	Funcionario funcionario = this.clinicService.findFuncionarioById(funcionarioId);
		if(funcionario == null){
			return new ResponseEntity<Funcionario>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Funcionario>(funcionario, HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.US_ADMIN)" )
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Funcionario> addFuncionario(@RequestBody @Valid Funcionario funcionario, BindingResult bindingResult, UriComponentsBuilder ucBuilder){
		BindingErrorsResponse errors = new BindingErrorsResponse();
		HttpHeaders headers = new HttpHeaders();
		if(bindingResult.hasErrors() || (funcionario == null)){
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<Funcionario>(headers, HttpStatus.BAD_REQUEST);
		}
		this.clinicService.saveFuncionario(funcionario);
		headers.setLocation(ucBuilder.path("/api/funcionarios/{id}").buildAndExpand(funcionario.getId()).toUri());
		return new ResponseEntity<Funcionario>(funcionario, headers, HttpStatus.CREATED);
	}

    @PreAuthorize( "hasRole(@roles.US_ADMIN)" )
	@RequestMapping(value = "/{funcionarioId}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Funcionario> updateFuncionario(@PathVariable("funcionarioId") int funcionarioId, @RequestBody @Valid Funcionario funcionario, BindingResult bindingResult){
		BindingErrorsResponse errors = new BindingErrorsResponse();
		HttpHeaders headers = new HttpHeaders();
		if(bindingResult.hasErrors() || (funcionario == null)){
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<Funcionario>(headers, HttpStatus.BAD_REQUEST);
		}
		Funcionario currentFuncionario = this.clinicService.findFuncionarioById(funcionarioId);
		if(currentFuncionario == null){
			return new ResponseEntity<Funcionario>(HttpStatus.NOT_FOUND);
		}
		currentFuncionario.setNomeCompleto(funcionario.getNomeCompleto());
		currentFuncionario.setRg(funcionario.getRg());
		currentFuncionario.setCpf(funcionario.getRg());
		currentFuncionario.setCpf(funcionario.getCpf());
		currentFuncionario.setEndereco(funcionario.getEndereco());
		currentFuncionario.setCep(funcionario.getCep());
		currentFuncionario.setDataNascimento(funcionario.getDataNascimento());
		currentFuncionario.setSexo(funcionario.getSexo());
		currentFuncionario.setTelefone(funcionario.getTelefone());
		currentFuncionario.setSetor(funcionario.getSetor());
		currentFuncionario.setFuncao(funcionario.getFuncao());
		currentFuncionario.setUnidadeSaude(funcionario.getUnidadeSaude());
		this.clinicService.saveFuncionario(currentFuncionario);
		return new ResponseEntity<Funcionario>(currentFuncionario, HttpStatus.NO_CONTENT);
	}

    @PreAuthorize( "hasRole(@roles.US_ADMIN)" )
	@RequestMapping(value = "/{funcionarioId}", method = RequestMethod.DELETE, produces = "application/json")
	@Transactional
	public ResponseEntity<Void> deleteFuncionario(@PathVariable("funcionarioId") int funcionarioId){
		Funcionario funcionario = this.clinicService.findFuncionarioById(funcionarioId);
		if(funcionario == null){
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		this.clinicService.deleteFuncionario(funcionario);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}

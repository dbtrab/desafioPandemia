package org.springframework.samples.petclinic.rest;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;

import org.springframework.samples.petclinic.model.UnidadeSaude;
import org.springframework.samples.petclinic.model.Funcionario;
import org.springframework.samples.petclinic.model.Prontuario;
import org.springframework.samples.petclinic.model.Paciente;
import org.springframework.samples.petclinic.model.Teste;
import org.springframework.samples.petclinic.model.TipoTeste;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * @author Eduarda Keller
 *
 */
public class JacksonCustomProntuarioSerializer extends StdSerializer<Prontuario> {

	public JacksonCustomProntuarioSerializer() {
		this(null);
	}

	public JacksonCustomProntuarioSerializer(Class<Prontuario> t) {
		super(t);
	}

	@Override
	public void serialize(Prontuario prontuario, JsonGenerator jgen, SerializerProvider provider) throws IOException {
		Format formatter = new SimpleDateFormat("dd/MM/yyyy");

        jgen.writeStartObject();//Prontuario
		if (prontuario.getId() == null) {
		    jgen.writeNullField("id");
        }
        else {
		    jgen.writeNumberField("id", prontuario.getId());
        }
        jgen.writeStringField("data", formatter.format(prontuario.getData()));

        Paciente paciente = prontuario.getPaciente();
        jgen.writeObjectFieldStart("paciente");
		if (paciente.getId() == null) {
			jgen.writeNullField("id");
		} else {
			jgen.writeNumberField("id", paciente.getId());
		}

		jgen.writeStringField("nomeCompleto", paciente.getNomeCompleto());
        jgen.writeStringField("rg", paciente.getRg());
        jgen.writeStringField("cpf", paciente.getCpf());
        jgen.writeStringField("endereco", paciente.getEndereco());
        jgen.writeStringField("cep", paciente.getCep());
        jgen.writeStringField("sexo", paciente.getSexo());
        jgen.writeStringField("telefone", paciente.getTelefone());
		jgen.writeStringField("dataNascimento", formatter.format(paciente.getDataNascimento()));
        jgen.writeStringField("nomeMae", paciente.getNomeMae());
        jgen.writeStringField("obs", paciente.getObs());
        jgen.writeStringField("nomeResponsavel", paciente.getNomeResponsavel());
        jgen.writeStringField("cpfResponsavel", paciente.getCpfResponsavel());
        jgen.writeStringField("telefoneResponsavel", paciente.getTelefoneResponsavel());
        jgen.writeEndObject(); // Paciente

        Funcionario funcionario = prontuario.getFuncionario();
        jgen.writeObjectFieldStart("funcionario");
        if (funcionario.getId() == null) {
            jgen.writeNullField("id");
        } else {
            jgen.writeNumberField("id", funcionario.getId());
        }
        jgen.writeStringField("nomeCompleto", funcionario.getNomeCompleto());
        jgen.writeStringField("rg", funcionario.getRg());
        jgen.writeStringField("cpf", funcionario.getCpf());
        jgen.writeStringField("endereco", funcionario.getEndereco());
        jgen.writeStringField("cep", funcionario.getCep());
        jgen.writeStringField("sexo", funcionario.getSexo());
        jgen.writeStringField("telefone", funcionario.getTelefone());
		jgen.writeStringField("dataNascimento", formatter.format(funcionario.getDataNascimento()));
        jgen.writeStringField("setor", funcionario.getSetor());
        jgen.writeStringField("funcao", funcionario.getFuncao());


        UnidadeSaude unidadeSaude = funcionario.getUnidadeSaude();
        jgen.writeObjectFieldStart("unidadeSaude");
        if (unidadeSaude.getId() == null) {
            jgen.writeNullField("id");
        } else {
            jgen.writeNumberField("id", unidadeSaude.getId());
        }
        jgen.writeStringField("nome", unidadeSaude.getNome());
        jgen.writeStringField("endereco", unidadeSaude.getEndereco());
        jgen.writeStringField("cep", unidadeSaude.getCep());
        jgen.writeStringField("telefone", unidadeSaude.getTelefone());
        jgen.writeEndObject(); // unidadeSaude
        jgen.writeEndObject(); // funcionario

        // write testes array
		jgen.writeArrayFieldStart("testes");
		for (Teste teste : prontuario.getTestes()) {
            jgen.writeStartObject(); // teste
            if (teste.getId() == null) {
			    jgen.writeNullField("id");
		    } else {
			    jgen.writeNumberField("id", teste.getId());
            }
            jgen.writeStringField("resultado", teste.getResultado());

            TipoTeste testeTipo = teste.getTipo();
		    jgen.writeObjectFieldStart("tipo");
		    if (testeTipo.getId() == null) {
			    jgen.writeNullField("id");
		    } else {
			    jgen.writeNumberField("id", testeTipo.getId());
		    }
		    jgen.writeStringField("name", testeTipo.getName());
            jgen.writeEndObject(); // tipo
            jgen.writeEndObject(); // teste
        }
        jgen.writeEndArray(); // testes
        jgen.writeEndObject(); // Prontuario



	}

}

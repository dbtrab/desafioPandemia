package org.springframework.samples.petclinic.rest;

import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.Paciente;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/pacientes")
public class PacienteRestController {

	@Autowired
	private ClinicService clinicService;

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/*/nome/{nome}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<Paciente>> getPacientesList(@PathVariable("nome") String nomePaciente) {
		if (nomePaciente == null) {
			nomePaciente = "";
		}
		Collection<Paciente> pacientes = this.clinicService.findPacienteByName(nomePaciente);
		if (pacientes.isEmpty()) {
			return new ResponseEntity<Collection<Paciente>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<Paciente>>(pacientes, HttpStatus.OK);
	}


    @PreAuthorize( "hasRole(@roles.ADMIN, @roles.FUNCIONARIO)" )
	@RequestMapping(value = "/{pacienteId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Paciente> getPaciente(@PathVariable("pacienteId") int pacienteId){
    	Paciente paciente = this.clinicService.findPacienteById(pacienteId);
		if(paciente == null){
			return new ResponseEntity<Paciente>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN, @roles.FUNCIONARIO)" )
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<Paciente>> getPacientes(){
		Collection<Paciente> pacientes = this.clinicService.findAllPacientes();
		if(pacientes.isEmpty()){
			return new ResponseEntity<Collection<Paciente>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<Paciente>>(pacientes, HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Paciente> addPaciente(@RequestBody @Valid Paciente paciente, BindingResult bindingResult, UriComponentsBuilder ucBuilder){
		BindingErrorsResponse errors = new BindingErrorsResponse();
		HttpHeaders headers = new HttpHeaders();
		if(bindingResult.hasErrors() || (paciente == null)){
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<Paciente>(headers, HttpStatus.BAD_REQUEST);
		}
		this.clinicService.savePaciente(paciente);
		headers.setLocation(ucBuilder.path("/api/pacientes/{id}").buildAndExpand(paciente.getId()).toUri());
		return new ResponseEntity<Paciente>(paciente, headers, HttpStatus.CREATED);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/{pacienteId}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Paciente> updatePaciente(@PathVariable("pacienteId") int pacienteId, @RequestBody @Valid Paciente paciente, BindingResult bindingResult){
		BindingErrorsResponse errors = new BindingErrorsResponse();
		HttpHeaders headers = new HttpHeaders();
		if(bindingResult.hasErrors() || (paciente == null)){
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<Paciente>(headers, HttpStatus.BAD_REQUEST);
		}
		Paciente currentPaciente = this.clinicService.findPacienteById(pacienteId);
		if(currentPaciente == null){
			return new ResponseEntity<Paciente>(HttpStatus.NOT_FOUND);
		}
		currentPaciente.setNomeCompleto(paciente.getNomeCompleto());
		currentPaciente.setRg(paciente.getRg());
		currentPaciente.setCpf(paciente.getRg());
		currentPaciente.setCpf(paciente.getCpf());
		currentPaciente.setEndereco(paciente.getEndereco());
		currentPaciente.setCep(paciente.getCep());
		currentPaciente.setDataNascimento(paciente.getDataNascimento());
		currentPaciente.setSexo(paciente.getSexo());
		currentPaciente.setTelefone(paciente.getTelefone());
		currentPaciente.setCns(paciente.getCns());
		currentPaciente.setNomeResponsavel(paciente.getNomeResponsavel());
		currentPaciente.setCpfResponsavel(paciente.getCpfResponsavel());
		currentPaciente.setTelefoneResponsavel(paciente.getTelefoneResponsavel());
		this.clinicService.savePaciente(currentPaciente);
		return new ResponseEntity<Paciente>(currentPaciente, HttpStatus.NO_CONTENT);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/{petId}", method = RequestMethod.DELETE, produces = "application/json")
	@Transactional
	public ResponseEntity<Void> deletePaciente(@PathVariable("pacienteId") int pacienteId){
		Paciente paciente = this.clinicService.findPacienteById(pacienteId);
		if(paciente == null){
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		this.clinicService.deletePaciente(paciente);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}


/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.rest;

import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.UnidadeSaude;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author Vitaliy Fedoriv
 *
 */

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("/api/unidadesSaude")
public class UnidadeSaudeRestController {

	@Autowired
	private ClinicService clinicService;

	@PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/*/nome/{nome}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<UnidadeSaude>> getUnidadesSaudeList(@PathVariable("nome") String nomeUnidadeSaude) {
		if (nomeUnidadeSaude == null) {
			nomeUnidadeSaude = "";
		}
		Collection<UnidadeSaude> unidadesSaude = this.clinicService.findUnidadeSaudeByName(nomeUnidadeSaude);
		if (unidadesSaude.isEmpty()) {
			return new ResponseEntity<Collection<UnidadeSaude>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<UnidadeSaude>>(unidadesSaude, HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<UnidadeSaude>> getUnidadesSaude() {
		Collection<UnidadeSaude> unidadesSaude = this.clinicService.findAllUnidadesSaude();
		if (unidadesSaude.isEmpty()) {
			return new ResponseEntity<Collection<UnidadeSaude>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<UnidadeSaude>>(unidadesSaude, HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/{unidadeSaudeId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<UnidadeSaude> getUnidadeSaude(@PathVariable("unidadeSaudeId") int unidadeSaudeId) {
		UnidadeSaude unidadeSaude = null;
		unidadeSaude = this.clinicService.findUnidadeSaudeById(unidadeSaudeId);
		if (unidadeSaude == null) {
			return new ResponseEntity<UnidadeSaude>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UnidadeSaude>(unidadeSaude, HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<UnidadeSaude> addUnidadeSaude(@RequestBody @Valid UnidadeSaude unidadeSaude, BindingResult bindingResult,
			UriComponentsBuilder ucBuilder) {
		HttpHeaders headers = new HttpHeaders();
		if (bindingResult.hasErrors() || unidadeSaude.getId() != null) {
            BindingErrorsResponse errors = new BindingErrorsResponse(unidadeSaude.getId());
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<UnidadeSaude>(headers, HttpStatus.BAD_REQUEST);
		}
		this.clinicService.saveUnidadeSaude(unidadeSaude);
		headers.setLocation(ucBuilder.path("/api/unidadeSaude/{id}").buildAndExpand(unidadeSaude.getId()).toUri());
		return new ResponseEntity<UnidadeSaude>(unidadeSaude, headers, HttpStatus.CREATED);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/{unidadeSaudeId}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<UnidadeSaude> updateUnidadeSaude(@PathVariable("unidadeSaudeId") int unidadeSaudeId, @RequestBody @Valid UnidadeSaude unidadeSaude,
			BindingResult bindingResult, UriComponentsBuilder ucBuilder) {
	    boolean bodyIdMatchesPathId = unidadeSaude.getId() == null || unidadeSaudeId == unidadeSaude.getId();
		if (bindingResult.hasErrors() || !bodyIdMatchesPathId) {
            BindingErrorsResponse errors = new BindingErrorsResponse(unidadeSaudeId, unidadeSaude.getId());
			errors.addAllErrors(bindingResult);
            HttpHeaders headers = new HttpHeaders();
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<UnidadeSaude>(headers, HttpStatus.BAD_REQUEST);
		}
		UnidadeSaude currentUnidadeSaude = this.clinicService.findUnidadeSaudeById(unidadeSaudeId);
		if (currentUnidadeSaude == null) {
			return new ResponseEntity<UnidadeSaude>(HttpStatus.NOT_FOUND);
		}
		currentUnidadeSaude.setNome(unidadeSaude.getNome());
		currentUnidadeSaude.setEndereco(unidadeSaude.getEndereco());
		currentUnidadeSaude.setCep(unidadeSaude.getCep());
		currentUnidadeSaude.setTelefone(unidadeSaude.getTelefone());
		this.clinicService.saveUnidadeSaude(currentUnidadeSaude);
		return new ResponseEntity<UnidadeSaude>(currentUnidadeSaude, HttpStatus.NO_CONTENT);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/{unidadeSaudeId}", method = RequestMethod.DELETE, produces = "application/json")
	@Transactional
	public ResponseEntity<Void> deleteUnidadeSaude(@PathVariable("unidadeSaudeId") int unidadeSaudeId) {
		UnidadeSaude unidadeSaude = this.clinicService.findUnidadeSaudeById(unidadeSaudeId);
		if (unidadeSaude == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		this.clinicService.deleteUnidadeSaude(unidadeSaude);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}



package org.springframework.samples.petclinic.rest;

import java.io.IOException;

import org.springframework.samples.petclinic.model.UnidadeSaude;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * @author Vitaliy Fedoriv
 *
 */

public class JacksonCustomUnidadeSaudeDeserializer extends StdDeserializer<UnidadeSaude> {

	public JacksonCustomUnidadeSaudeDeserializer(){
		this(null);
	}

	public JacksonCustomUnidadeSaudeDeserializer(Class<UnidadeSaude> t) {
		super(t);
	}

	@Override
	public UnidadeSaude deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
		JsonNode node = parser.getCodec().readTree(parser);
		UnidadeSaude unidadeSaude = new UnidadeSaude();
		String nome = node.get("nome").asText(null);
		String endereco = node.get("endereco").asText(null);
		String cep = node.get("cep").asText(null);
		String telefone = node.get("telefone").asText(null);
		if (node.hasNonNull("id")) {
			unidadeSaude.setId(node.get("id").asInt());
		}
        unidadeSaude.setNome(nome);
        unidadeSaude.setEndereco(endereco);
        unidadeSaude.setCep(cep);
        unidadeSaude.setTelefone(telefone);
		return unidadeSaude;
	}

}

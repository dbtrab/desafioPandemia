/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.rest;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.Teste;
import org.springframework.samples.petclinic.model.TipoTeste;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/testes")
public class TesteRestController {

	@Autowired
	private ClinicService clinicService;

    @PreAuthorize( "hasAnyRole(@roles.ADMIN, @roles.UNIDADESAUDE, @roles.FUNCIONARIO)" )
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<Teste>> getAllTestes(){
		Collection<Teste> testes = new ArrayList<Teste>();
		testes.addAll(this.clinicService.findAllTestes());
		if (testes.isEmpty()){
			return new ResponseEntity<Collection<Teste>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<Teste>>(testes, HttpStatus.OK);
	}

    @PreAuthorize( "hasAnyRole(@roles.ADMIN, @roles.UNIDADESAUDE, @roles.FUNCIONARIO)" )
	@RequestMapping(value = "/{testeId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Teste> getTeste(@PathVariable("testeId") int testeId){
		Teste teste = this.clinicService.findTesteById(testeId);
		if(teste == null){
			return new ResponseEntity<Teste>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Teste>(teste, HttpStatus.OK);
    }

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/tipoTestes", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<TipoTeste>> getTipoTestes(){
		return new ResponseEntity<Collection<TipoTeste>>(this.clinicService.findTipoTestes(), HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Teste> addTeste(@RequestBody @Valid Teste teste, BindingResult bindingResult, UriComponentsBuilder ucBuilder){
		BindingErrorsResponse errors = new BindingErrorsResponse();
		HttpHeaders headers = new HttpHeaders();
		if(bindingResult.hasErrors() || (teste == null)){
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<Teste>(headers, HttpStatus.BAD_REQUEST);
		}
		this.clinicService.saveTeste(teste);
		headers.setLocation(ucBuilder.path("/api/Testes/{id}").buildAndExpand(teste.getId()).toUri());
		return new ResponseEntity<Teste>(teste, headers, HttpStatus.CREATED);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/{testeId}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Teste> updateTeste(@PathVariable("testeId") int testeId, @RequestBody @Valid Teste teste, BindingResult bindingResult){
		BindingErrorsResponse errors = new BindingErrorsResponse();
		HttpHeaders headers = new HttpHeaders();
		if(bindingResult.hasErrors() || (teste == null)){
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<Teste>(headers, HttpStatus.BAD_REQUEST);
		}
		Teste currentTeste = this.clinicService.findTesteById(testeId);
		if(currentTeste == null){
			return new ResponseEntity<Teste>(HttpStatus.NOT_FOUND);
		}
        currentTeste.setResultado(teste.getResultado());
        currentTeste.setTipo(teste.getTipo());
        currentTeste.setProntuario(teste.getProntuario());
		this.clinicService.saveTeste(currentTeste);
		return new ResponseEntity<Teste>(currentTeste, HttpStatus.NO_CONTENT);
	}

    @PreAuthorize( "hasRole(@roles.ADMIN)" )
	@RequestMapping(value = "/{testeId}", method = RequestMethod.DELETE, produces = "application/json")
	@Transactional
	public ResponseEntity<Void> deleteTeste(@PathVariable("testeId") int testeId){
		Teste teste = this.clinicService.findTesteById(testeId);
		if(teste == null){
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		this.clinicService.deleteTeste(teste);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}

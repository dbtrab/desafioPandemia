/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.rest;

import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.Prontuario;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author Vitaliy Fedoriv
 *
 */

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("/api/prontuarios")
public class ProntuarioRestController {

	@Autowired
	private ClinicService clinicService;

    @PreAuthorize( "hasAnyRole(@roles.FUNCIONARIO, @roles.UNIDADESAUDE)" )
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<Prontuario>> getProntuarios() {
		Collection<Prontuario> prontuarios = this.clinicService.findAllProntuarios();
		if (prontuarios.isEmpty()) {
			return new ResponseEntity<Collection<Prontuario>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<Prontuario>>(prontuarios, HttpStatus.OK);
	}

    @PreAuthorize( "hasAnyRole(@roles.FUNCIONARIO, @roles.UNIDADESAUDE)" )
	@RequestMapping(value = "/{prontuarioId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Prontuario> getProntuario(@PathVariable("prontuarioId") int prontuarioId) {
		Prontuario prontuario = null;
		prontuario = this.clinicService.findProntuarioById(prontuarioId);
		if (prontuario == null) {
			return new ResponseEntity<Prontuario>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Prontuario>(prontuario, HttpStatus.OK);
	}

    @PreAuthorize( "hasRole(@roles.FUNCIONARIO)" )
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Prontuario> addProntuario(@RequestBody @Valid Prontuario prontuario, BindingResult bindingResult,
			UriComponentsBuilder ucBuilder) {
		HttpHeaders headers = new HttpHeaders();
		if (bindingResult.hasErrors() || prontuario.getId() != null) {
            BindingErrorsResponse errors = new BindingErrorsResponse(prontuario.getId());
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<Prontuario>(headers, HttpStatus.BAD_REQUEST);
		}
		this.clinicService.saveProntuario(prontuario);
		headers.setLocation(ucBuilder.path("/api/prontuarios/{id}").buildAndExpand(prontuario.getId()).toUri());
		return new ResponseEntity<Prontuario>(prontuario, headers, HttpStatus.CREATED);
	}

    @PreAuthorize( "hasRole(@roles.FUNCIONARIO)" )
	@RequestMapping(value = "/{prontuarioId}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Prontuario> updateProntuario(@PathVariable("prontuarioId") int prontuarioId, @RequestBody @Valid Prontuario prontuario,
			BindingResult bindingResult, UriComponentsBuilder ucBuilder) {
	    boolean bodyIdMatchesPathId = prontuario.getId() == null || prontuarioId == prontuario.getId();
		if (bindingResult.hasErrors() || !bodyIdMatchesPathId) {
            BindingErrorsResponse errors = new BindingErrorsResponse(prontuarioId, prontuario.getId());
			errors.addAllErrors(bindingResult);
            HttpHeaders headers = new HttpHeaders();
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<Prontuario>(headers, HttpStatus.BAD_REQUEST);
		}
		Prontuario currentProntuario = this.clinicService.findProntuarioById(prontuarioId);
		if (currentProntuario == null) {
			return new ResponseEntity<Prontuario>(HttpStatus.NOT_FOUND);
		}
        currentProntuario.setData(prontuario.getData());
        currentProntuario.setPaciente(prontuario.getPaciente());
        currentProntuario.setFuncionario(prontuario.getFuncionario());
		this.clinicService.saveProntuario(currentProntuario);
		return new ResponseEntity<Prontuario>(currentProntuario, HttpStatus.NO_CONTENT);
	}

    @PreAuthorize( "hasRole(@roles.UNIDADESAUDE)" )
	@RequestMapping(value = "/{prontuarioId}", method = RequestMethod.DELETE, produces = "application/json")
	@Transactional
	public ResponseEntity<Void> deleteProntuario(@PathVariable("prontuarioId") int prontuarioId) {
		Prontuario prontuario = this.clinicService.findProntuarioById(prontuarioId);
		if (prontuario == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		this.clinicService.deleteProntuario(prontuario);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}

package org.springframework.samples.petclinic.model;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Collections;

import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.samples.petclinic.rest.JacksonCustomFuncionarioSerializer;
//import org.springframework.samples.petclinic.rest.JacksonCustomFuncionarioDeserializer;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import javax.validation.constraints.NotEmpty;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Eduarda Keller
 */
@Entity
@JsonSerialize(using = JacksonCustomFuncionarioSerializer.class)
@Table(name = "funcionarios")
public class Funcionario extends Pessoa {

	@Column(name = "setor")
	@NotEmpty
	protected String setor;

	@Column(name = "funcao")
	@NotEmpty
	protected String funcao;

	 @ManyToOne
	 @JoinColumn(name = "unidadeSaude_id")
     private UnidadeSaude unidadeSaude;

     @OneToMany(cascade = CascadeType.ALL, mappedBy = "funcionario", fetch = FetchType.EAGER)
     private Set<Prontuario> prontuarios;

	 public String getSetor() {
		return setor;
	}

	public String getFuncao() {
		return funcao;
	}

	 public UnidadeSaude getUnidadeSaude(){
	        return this.unidadeSaude;
	    }

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
    }

    public void setUnidadeSaude(UnidadeSaude unidadeSaude) {
        this.unidadeSaude = unidadeSaude;
    }

    @JsonIgnore
    protected Set<Prontuario> getProntuariosInternal() {
        if (this.prontuarios == null) {
            this.prontuarios = new HashSet<>();
        }
        return this.prontuarios;
    }

    protected void setProntuariosInternal(Set<Prontuario> prontuarios) {
        this.prontuarios = prontuarios;
    }

    public void addProntuario(Prontuario prontuario) {
        getProntuariosInternal().add(prontuario);
        prontuario.setFuncionario(this);
    }

    public List<Prontuario> getProntuarios() {
        List<Prontuario> sortedProntuarios = new ArrayList<>(getProntuariosInternal());
        PropertyComparator.sort(sortedProntuarios, new MutableSortDefinition("nome", true, true));
        return Collections.unmodifiableList(sortedProntuarios);
    }

        /**
     * Return the Funcionario with the given name, or null if none found for this UnidadeSaude.
     *
     * @param nome to test
     * @return true if prontuario nome is already in use
     */
    public Prontuario getProntuario(String nome) {
        return getProntuario(nome, false);
    }

        /**
     * Return the Funcionario with the given name, or null if none found for this UnidadeSaude.
     *
     * @param name to test
     * @return true if Funcionario name is already in use
     */
    public Prontuario getProntuario(String nome, boolean ignoreNew) {
        nome = nome.toLowerCase();
        for (Prontuario prontuario : getProntuariosInternal()) {
            if (!ignoreNew || !prontuario.isNew()) {
                String compName = prontuario.getPaciente().getNomeCompleto();
                compName = compName.toLowerCase();
                if (compName.equals(nome)) {
                    return prontuario;
                }
            }
        }
        return null;
    }


}

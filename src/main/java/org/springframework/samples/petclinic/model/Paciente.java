package org.springframework.samples.petclinic.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;

import org.springframework.samples.petclinic.rest.JacksonCustomPacienteSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.MapsId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Eduarda Keller
 */
@Entity
@JsonSerialize(using = JacksonCustomPacienteSerializer.class)
@Table(name = "pacientes")
public class Paciente extends Pessoa {

	@Column(name = "cns")
	@NotEmpty
	@Digits(fraction = 0, integer = 15)
    protected String cns;

	@Column(name = "nomeMae")
	@NotEmpty
	protected String nomeMae;

	@Column(name = "obs")
	private String obs;

	@Column(name = "nomeResponsavel")
	@NotEmpty
	private String nomeResponsavel;

	@Column(name = "cpfResponsavel")
	@NotEmpty
	@Digits(fraction = 0, integer = 11)
	private String cpfResponsavel;

	@Column(name = "telefoneResponsavel")
	@NotEmpty
	@Digits(fraction = 0, integer = 10)
    private String telefoneResponsavel;

    @OneToOne(mappedBy = "paciente")
    private Prontuario prontuario;

	public void setCns(String cns) {
		this.cns = cns;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	public void setCpfResponsavel(String cpfResponsavel) {
		this.cpfResponsavel = cpfResponsavel;
	}

	public void setTelefoneResponsavel(String telefoneResponsavel) {
		this.telefoneResponsavel = telefoneResponsavel;
	}

	public String getCns() {
		return cns;
	}

	public String getObs() {
		return obs;
	}

	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	public String getCpfResponsavel() {
		return cpfResponsavel;
	}

	public String getTelefoneResponsavel() {
		return telefoneResponsavel;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
    }

    public Prontuario getProntuario(){
        return this.prontuario;
    }

    public void setProntuario(Prontuario prontuario){
        this.prontuario = prontuario;
    }
}

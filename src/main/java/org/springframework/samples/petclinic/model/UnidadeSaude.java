/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.model;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Collections;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;


import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.samples.petclinic.rest.JacksonCustomUnidadeSaudeSerializer;
import org.springframework.samples.petclinic.rest.JacksonCustomUnidadeSaudeDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;




/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Daniel Cierco
 */
@Entity
@JsonSerialize(using = JacksonCustomUnidadeSaudeSerializer.class)
@JsonDeserialize(using = JacksonCustomUnidadeSaudeDeserializer.class)
@Table(name = "unidadesSaude")
public class UnidadeSaude extends BaseEntity {

    @Column(name = "nome")
    @NotEmpty
    protected String nome;

    @Column(name = "endereco")
    @NotEmpty
    private String endereco;

    @Column(name = "cep")
    @NotEmpty
    @Digits(fraction = 0, integer = 8)
    private String cep;

    @Column(name = "telefone")
    @NotEmpty
    @Digits(fraction = 0, integer = 10)
    private String telefone;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unidadeSaude", fetch = FetchType.EAGER)
    private Set<Funcionario> funcionarios;

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCep() {
        return this.cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @JsonIgnore
    protected Set<Funcionario> getFuncionariosInternal() {
        if (this.funcionarios == null) {
            this.funcionarios = new HashSet<>();
        }
        return this.funcionarios;
    }

    protected void setFuncionariosInternal(Set<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public void addFuncionario(Funcionario funcionario) {
        getFuncionariosInternal().add(funcionario);
        funcionario.setUnidadeSaude(this);
    }

    public List<Funcionario> getFuncionarios() {
        List<Funcionario> sortedFuncionarios = new ArrayList<>(getFuncionariosInternal());
        PropertyComparator.sort(sortedFuncionarios, new MutableSortDefinition("name", true, true));
        return Collections.unmodifiableList(sortedFuncionarios);
    }

        /**
     * Return the Funcionario with the given name, or null if none found for this UnidadeSaude.
     *
     * @param name to test
     * @return true if funcionario name is already in use
     */
    public Funcionario getFuncionario(String name) {
        return getFuncionario(name, false);
    }

        /**
     * Return the Funcionario with the given name, or null if none found for this UnidadeSaude.
     *
     * @param name to test
     * @return true if Funcionario name is already in use
     */
    public Funcionario getFuncionario(String name, boolean ignoreNew) {
        name = name.toLowerCase();
        for (Funcionario funcionario : getFuncionariosInternal()) {
            if (!ignoreNew || !funcionario.isNew()) {
                String compName = funcionario.getNomeCompleto();
                compName = compName.toLowerCase();
                if (compName.equals(name)) {
                    return funcionario;
                }
            }
        }
        return null;
    }


}

/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.model;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Collections;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;

import org.springframework.samples.petclinic.rest.JacksonCustomTesteSerializer;
//import org.springframework.samples.petclinic.rest.JacksonCustomTesteDeserializer;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Daniel Cierco
 */
@Entity
@JsonSerialize(using = JacksonCustomTesteSerializer.class)
@Table(name = "testes")
public class Teste extends BaseEntity {

    @Column(name = "resultado")
    @NotEmpty
    protected String resultado;


    @ManyToOne
    @JoinColumn(name = "tipo_id")
    private TipoTeste tipo;

    @ManyToOne
    @JoinColumn(name = "prontuario_id")
    private Prontuario prontuario;

    public Prontuario getProntuario(){
        return this.prontuario;
    }

    public void setProntuario(Prontuario prontuario){
        this.prontuario = prontuario;
    }

    public TipoTeste getTipo() {
        return this.tipo;
    }

    public void setTipo(TipoTeste tipo) {
        this.tipo = tipo;
    }

    public String getResultado() {
        return this.resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

}

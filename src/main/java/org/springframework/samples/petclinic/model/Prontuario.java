package org.springframework.samples.petclinic.model;

import java.util.Date;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Collections;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import javax.persistence.OneToMany;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;

import org.springframework.samples.petclinic.rest.JacksonCustomProntuarioSerializer;
//import org.springframework.samples.petclinic.rest.JacksonCustomProntuarioDeserializer;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Simple JavaBean domain object representing an person.
 *
 * @author Eduarda Keller
 */
@Entity
@JsonSerialize(using = JacksonCustomProntuarioSerializer.class)
@Table(name = "prontuarios")
public class Prontuario extends BaseEntity {

    @Column(name = "prontuarios_data")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date data;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "paciente_id", referencedColumnName = "id")
    private Paciente paciente;

	@ManyToOne
	@JoinColumn(name = "funcionario_id")
    private Funcionario funcionario;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "prontuario", fetch = FetchType.EAGER)
    private Set<Teste> testes;

    public Date getData(){
        return this.data;
    }

    public void setData(Date data){
        this.data = data;
    }

    public Paciente getPaciente(){
        return this.paciente;
    }

    public void setPaciente(Paciente paciente){
        this.paciente = paciente;
    }

    public Funcionario getFuncionario(){
        return this.funcionario;
    }

    public void setFuncionario(Funcionario funcionario){
        this.funcionario = funcionario;
    }

    @JsonIgnore
    protected Set<Teste> getTestesInternal() {
        if (this.testes == null) {
            this.testes = new HashSet<>();
        }
        return this.testes;
    }

    protected void setTestesInternal(Set<Teste> testes) {
        this.testes = testes;
    }

    public void addTeste(Teste teste) {
        getTestesInternal().add(teste);
        teste.setProntuario(this);
    }

    public List<Teste> getTestes() {
        List<Teste> sortedTestes = new ArrayList<>(getTestesInternal());
        PropertyComparator.sort(sortedTestes, new MutableSortDefinition("name", true, true));
        return Collections.unmodifiableList(sortedTestes);
    }

}

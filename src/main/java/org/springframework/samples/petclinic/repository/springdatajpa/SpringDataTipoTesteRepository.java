package org.springframework.samples.petclinic.repository.springdatajpa;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.Repository;
import org.springframework.samples.petclinic.model.TipoTeste;
import org.springframework.samples.petclinic.repository.TipoTesteRepository;

@Profile("spring-data-jpa")
public interface SpringDataTipoTesteRepository extends TipoTesteRepository, Repository<TipoTeste, Integer>, TipoTesteRepositoryOverride{

}

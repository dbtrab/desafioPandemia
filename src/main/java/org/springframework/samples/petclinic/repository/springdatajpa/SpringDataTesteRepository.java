package org.springframework.samples.petclinic.repository.springdatajpa;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.samples.petclinic.model.Teste;
import org.springframework.samples.petclinic.model.TipoTeste;
import org.springframework.samples.petclinic.repository.TesteRepository;

@Profile("spring-data-jpa")
public interface SpringDataTesteRepository extends TesteRepository, Repository<Teste, Integer>, TesteRepositoryOverride {

    @Override
    @Query("SELECT tTeste FROM TipoTeste tTeste ORDER BY tTeste.name")
    List<TipoTeste> findTipoTestes() throws DataAccessException;
}

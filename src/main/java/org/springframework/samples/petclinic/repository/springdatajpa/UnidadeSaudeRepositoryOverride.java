package org.springframework.samples.petclinic.repository.springdatajpa;

import org.springframework.samples.petclinic.model.UnidadeSaude;

public interface UnidadeSaudeRepositoryOverride {
	void delete(UnidadeSaude unidadeSaude);

}

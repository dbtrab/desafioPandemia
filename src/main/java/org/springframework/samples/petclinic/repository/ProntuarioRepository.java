package org.springframework.samples.petclinic.repository;

import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Prontuario;

public interface ProntuarioRepository {
		
	Collection<Prontuario> findAll() throws DataAccessException;
    
	Prontuario findById(int id) throws DataAccessException;

	void save(Prontuario prontuario) throws DataAccessException;
	
	void delete(Prontuario prontuario) throws DataAccessException;
}

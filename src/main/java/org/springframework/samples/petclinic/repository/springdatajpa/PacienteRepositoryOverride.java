package org.springframework.samples.petclinic.repository.springdatajpa;

import org.springframework.context.annotation.Profile;
import org.springframework.samples.petclinic.model.Paciente;

/**
 * @author Eduarda Keller
 *
 */

@Profile("spring-data-jpa")
public interface PacienteRepositoryOverride {

	void delete(Paciente paciente);

}

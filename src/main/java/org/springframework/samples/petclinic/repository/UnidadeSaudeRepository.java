package org.springframework.samples.petclinic.repository;

import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.UnidadeSaude;

public interface UnidadeSaudeRepository {

	UnidadeSaude findById(int id) throws DataAccessException;

	void save(UnidadeSaude unidadeSaude) throws DataAccessException;

	Collection<UnidadeSaude> findAll() throws DataAccessException;

    void delete(UnidadeSaude unidadeSaude) throws DataAccessException;

    Collection<UnidadeSaude> findByName(String nome) throws DataAccessException;


}

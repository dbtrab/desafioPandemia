package org.springframework.samples.petclinic.repository.springdatajpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.samples.petclinic.model.Teste;
import org.springframework.samples.petclinic.model.TipoTeste;

@Profile("spring-data-jpa")
public class SpringDataTipoTesteRepositoryImpl implements TipoTesteRepositoryOverride{
	@PersistenceContext
    private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public void delete(TipoTeste tipoTeste) {
        this.em.remove(this.em.contains(tipoTeste) ? tipoTeste : this.em.merge(tipoTeste));
		Integer tipoTesteId = tipoTeste.getId();
		
		List<Teste> testes = this.em.createQuery("SELECT pet FROM Teste teste WHERE type_id=" + tipoTesteId).getResultList();
		for (Teste teste : testes){
			this.em.createQuery("DELETE FROM Teste teste WHERE id=" + teste.getId()).executeUpdate();
		}
		this.em.createQuery("DELETE FROM TipoTeste tipoteste WHERE id=" + tipoTesteId).executeUpdate();
	}
	

}

package org.springframework.samples.petclinic.repository;

import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Paciente;


	/**
	 *
	 * @author Eduarda Keller
	 */
public interface PacienteRepository {

	Paciente findById(int id) throws DataAccessException;

	void save(Paciente paciente) throws DataAccessException;

	Collection<Paciente> findAll() throws DataAccessException;

    void delete(Paciente paciente) throws DataAccessException;

    Collection<Paciente> findByName(String nome) throws DataAccessException;

}

package org.springframework.samples.petclinic.repository;

import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.TipoTeste;

public interface TipoTesteRepository {
	Collection<TipoTeste> findAll() throws DataAccessException;

	TipoTeste findById(int id) throws DataAccessException;

	void save(TipoTeste tipoTeste) throws DataAccessException;

    void delete(TipoTeste tipoTeste) throws DataAccessException;

}

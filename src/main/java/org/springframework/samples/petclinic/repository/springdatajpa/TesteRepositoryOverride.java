package org.springframework.samples.petclinic.repository.springdatajpa;

import org.springframework.context.annotation.Profile;
import org.springframework.samples.petclinic.model.Teste;

/**
 * @author Vitaliy Fedoriv
 *
 */

@Profile("spring-data-jpa")
public interface TesteRepositoryOverride {

	void delete(Teste teste);

}

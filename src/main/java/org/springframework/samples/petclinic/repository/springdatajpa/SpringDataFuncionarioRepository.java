package org.springframework.samples.petclinic.repository.springdatajpa;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.Repository;
import org.springframework.samples.petclinic.model.Funcionario;
import org.springframework.samples.petclinic.repository.FuncionarioRepository;

/**
 * Spring Data JPA specialization of the {@link PetRepository} interface
 *
 * @author Eduarda Keller
 */

@Profile("spring-data-jpa")
public interface SpringDataFuncionarioRepository extends FuncionarioRepository, Repository<Funcionario, Integer> {

}

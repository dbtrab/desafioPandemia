package org.springframework.samples.petclinic.repository.springdatajpa;

import java.util.Collection;

import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.samples.petclinic.model.Paciente;
import org.springframework.samples.petclinic.repository.PacienteRepository;

/**
 * @author Eduarda Keller
 *
 */

@Profile("spring-data-jpa")
public interface SpringDataPacienteRepository extends PacienteRepository, Repository<Paciente, Integer> {

    @Override
    @Query("SELECT DISTINCT paciente FROM Paciente paciente WHERE paciente.nomeCompleto LIKE :nome%")
    Collection<Paciente> findByName(@Param("nome") String nome);

    @Override
    @Query("SELECT paciente FROM Paciente paciente WHERE paciente.id =:id")
    Paciente findById(@Param("id") int id);

}

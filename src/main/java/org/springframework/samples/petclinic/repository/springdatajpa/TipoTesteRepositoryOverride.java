package org.springframework.samples.petclinic.repository.springdatajpa;

import org.springframework.context.annotation.Profile;
import org.springframework.samples.petclinic.model.TipoTeste;

@Profile("spring-data-jpa")
public interface TipoTesteRepositoryOverride {
	
	void delete(TipoTeste tipoTeste);
}

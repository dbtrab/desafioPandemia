package org.springframework.samples.petclinic.repository.springdatajpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.samples.petclinic.model.Paciente;

/**
 * @author Eduarda Keller
 *
 */

@Profile("spring-data-jpa")
public class SpringDataPacienteRepositoryImpl implements PacienteRepositoryOverride {

	@PersistenceContext
    private EntityManager em;

	@Override
	public void delete(Paciente paciente) {
		String pacienteId = paciente.getId().toString();
		this.em.createQuery("DELETE FROM prontuarios prontuatio WHERE id=" + pacienteId).executeUpdate();
        if (em.contains(paciente)) {
            em.remove(paciente);
        }
	}

}

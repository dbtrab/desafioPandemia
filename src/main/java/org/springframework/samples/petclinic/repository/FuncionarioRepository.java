package org.springframework.samples.petclinic.repository;

import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Funcionario;

/**
 *
 * @author Eduarda Keller
 */

public interface FuncionarioRepository {
	
	Collection<Funcionario> findAll() throws DataAccessException;
    
	Funcionario findById(int id) throws DataAccessException;

	void save(Funcionario func) throws DataAccessException;
	
	void delete(Funcionario func) throws DataAccessException;
}

package org.springframework.samples.petclinic.repository.springdatajpa;

import java.util.Collection;

import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.samples.petclinic.model.UnidadeSaude;
import org.springframework.samples.petclinic.repository.UnidadeSaudeRepository;

@Profile("spring-data-jpa")
public interface SpringDataUnidadeSaudeRepository extends UnidadeSaudeRepository, Repository<UnidadeSaude, Integer> {

    @Override
    @Query("SELECT DISTINCT unidadeSaude FROM UnidadeSaude unidadeSaude WHERE unidadeSaude.nome LIKE :nome%")
    Collection<UnidadeSaude> findByName(@Param("nome") String nome);

    @Override
    @Query("SELECT unidadeSaude FROM UnidadeSaude unidadeSaude left join fetch unidadeSaude.funcionarios WHERE unidadeSaude.id =:id")
    UnidadeSaude findById(@Param("id") int id);
}

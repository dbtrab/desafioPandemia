package org.springframework.samples.petclinic.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Teste;
import org.springframework.samples.petclinic.model.TipoTeste;

public interface TesteRepository {
	Collection<Teste> findAll() throws DataAccessException;

	Teste findById(int id) throws DataAccessException;

	void save(Teste teste) throws DataAccessException;

    void delete(Teste teste) throws DataAccessException;

    List<TipoTeste> findTipoTestes() throws DataAccessException;
}

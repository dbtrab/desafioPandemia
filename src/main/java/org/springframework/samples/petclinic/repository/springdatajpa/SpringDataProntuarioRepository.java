package org.springframework.samples.petclinic.repository.springdatajpa;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.Repository;
import org.springframework.samples.petclinic.model.Prontuario;
import org.springframework.samples.petclinic.repository.ProntuarioRepository;

@Profile("spring-data-jpa")
public interface SpringDataProntuarioRepository extends ProntuarioRepository, Repository<Prontuario, Integer>{

}
